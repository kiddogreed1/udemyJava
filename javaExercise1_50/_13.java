package javaExercise1_50;

// TODO: 02/02/2023  Write a Java program to print the area and perimeter of a rectangle.
//
//Test Data:
//        Width = 5.5 Height = 8.5
//
//        Expected Output
//        Area is 5.6 * 8.5 = 47.60
//        Perimeter is 2 * (5.6 + 8.5) = 28.20

import java.util.Scanner;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class _13 {
    private static final DecimalFormat df = new DecimalFormat("0.00");
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println( "welcome to area/perimeter calc: enter L and W in order");

        double firstNum = sc.nextDouble();
        System.out.print("(L)"+firstNum+", ");
        double secondNum = sc.nextDouble();
        System.out.print("(W)"+secondNum);
        System.out.println("CALCULATING ......");
        double area = rectangleArea(firstNum,secondNum);
        System.out.println(" area :"+df.format(area));

        double perimeter = rectPerimeter(firstNum, secondNum);
        System.out.println(" perimeter:"+df.format(perimeter));


    }
    public static double rectangleArea(double x, double y){
        return  (x*2)+(y*2);
    }

    public static double rectPerimeter(double x, double y){
        return x*y;
    }
}
