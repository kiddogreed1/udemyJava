package javaExercise1_50;

// TODO: 07/02/2023 Write a Java program to multiply two binary numbers.
//
//Input Data:
//        Input the first binary number: 10
//        Input the second binary number: 11
//        Expected Output
//
//        Product of two binary numbers: 110
public class _18 {

    public static void main(String[] args) {

        int a = 10;
        int b = 11;

        String s1 = Integer.toString(a);
        String s2 = Integer.toString(b);
        int number0 = Integer.parseInt(s1, 2);
        int number1 = Integer.parseInt(s2, 2);

        int sum = number0 * number1;
        String s3 = Integer.toBinaryString(sum);
        System.out.println(s3);
    }


}
