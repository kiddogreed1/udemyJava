package javaExercise1_50;

import java.util.Scanner;
import java.util.logging.Logger;

// TODO: 09/02/2023 Write a Java program to convert a octal number to a decimal number
//Input Data:
//        Input any octal number: 10
//        Expected Output
//
//        Equivalent decimal number: 8
public class _25 {

    public static void main(String[] args) {
       // Scanner sc = new Scanner(System.in);

        System.out.print("Enter an octal number: ");
//        String octal = sc.nextLine();
        String octal = "10";
        int decimal = 0;

        for (int i = 0; i < octal.length(); i++) {
            decimal = decimal * 8 + Character.getNumericValue(octal.charAt(i));
        }

        System.out.println("The decimal equivalent is: " + decimal);
       // sc.close();
    }
}
