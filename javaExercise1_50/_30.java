package javaExercise1_50;

public class _30 {

//   Input Data:
// Input a hexadecimal number: 40
// Expected Output

// Equivalent of octal number is: 100 
    public static void main(String[] args) {
      
      
      System.out.print("Enter a hexadecimal number: ");
      String hex = "40";
      
      // Convert the hexadecimal number to decimal
      int decimal = Integer.parseInt(hex, 16);
      
      // Convert the decimal number to octal
      String octal = Integer.toOctalString(decimal);
      
      System.out.println("Octal equivalent of " + hex + " is " + octal);
    }
}
