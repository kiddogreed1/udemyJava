package javaExercise1_50;

// TODO: 07/02/2023 Write a Java program to convert a decimal number to octal number.
//Input Data:
//        Input a Decimal Number: 15
//        Expected Output
//
//        Octal number is: 17
public class _21 {
    public static void main(String[] args) {
        int a = 15;

        System.out.println((Integer.toOctalString(a)));
    }
}
