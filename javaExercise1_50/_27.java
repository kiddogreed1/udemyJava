package javaExercise1_50;

import java.util.Scanner;
//
//Input Data:
//        Input a octal number : 100
//        Expected Output
//
//        Equivalent hexadecimal number: 40

public class _27 {
    public static void main(String[] args) {
       // Scanner scanner = new Scanner(System.in);
        // Read the octal number from the user
       // int
        System.out.print("Enter an octal number: ");
        String octalStr = "100";

        // Convert the octal number to decimal
        int decimal = Integer.parseInt(octalStr, 8);

        // Convert the decimal number to hexadecimal
        String hexadecimal = Integer.toHexString(decimal);

        // Display the result
        System.out.println("The hexadecimal equivalent of the octal number is " + hexadecimal.toUpperCase());
    }
}
