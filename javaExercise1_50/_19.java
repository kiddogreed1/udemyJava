package javaExercise1_50;

// TODO: 07/02/2023 Write a Java program to convert a decimal number to binary number
//Input Data:
//        Input a Decimal Number : 5
//        Expected Output
//
//        Binary number is: 101
public class _19 {
    public static void main(String[] args) {

        int a = 5;
        System.out.println(Integer.toBinaryString(Integer.parseInt(Integer.toString(a))));
    }
}
