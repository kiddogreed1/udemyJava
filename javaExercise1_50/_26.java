
package javaExercise1_50;

import java.util.Scanner;


/**
 * _26
 */
public class _26 {

  //Write a Java program to convert a octal number to a binary number.

// Input Data:
// Input any octal number: 7
// Expected Output

// Equivalent binary number: 111 

  public static void main(String[] args) {
    //scanner for input octal number
    Scanner scanner = new Scanner(System.in);

        // Input an octal number
        System.out.print("Enter an octal number: ");
        int octal = scanner.nextInt();



        // Convert the octal number to binary
        int decimal = 0, power = 0;
        while (octal != 0) {
            decimal += (octal % 10) * Math.pow(8, power);
            power++;
            octal /= 10;
        }

        int binary = 0, place = 1;
        while (decimal != 0) {
            binary += (decimal % 2) * place;
            place *= 10;
            decimal /= 2;
        }

        // Output the binary number
        System.out.println("The binary equivalent is: " + binary);
        scanner.close();
    
  }

//   We first take an octal number as input from the user using the Scanner class.

// We then convert the octal number to decimal by multiplying each digit with the corresponding power of 8 and adding the products.

// We then convert the decimal number to binary by dividing it by 2 and keeping track of the remainders until the decimal number becomes 0. The remainders should be read in reverse order to get the binary equivalent.

// Finally, we output the binary equivalent of the octal number.
}