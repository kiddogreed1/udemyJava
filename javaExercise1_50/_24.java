package javaExercise1_50;

import java.util.Scanner;

// TODO: 08/02/2023 Write a Java program to convert a binary number to a Octal number.
//
//Input Data:
//        Input a Binary Number: 111
//        Expected Output
public class _24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a binary number: ");
        String binary = sc.nextLine();
        int decimal = Integer.parseInt(binary, 2);
        String octal  = Integer.toOctalString(decimal);
        System.out.println("Octal equivalent: " + octal);
    }
}
