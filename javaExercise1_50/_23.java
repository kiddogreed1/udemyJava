package javaExercise1_50;
import java.util.Scanner;
// TODO: 08/02/2023 Write a Java program to convert a binary number to hexadecimal number.
//
//Input Data:
//        Input a Binary Number: 1101
//        Expected Output
//
//        HexaDecimal value: D

public class _23 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a binary number: ");
        String binary = sc.nextLine();
        int decimal = Integer.parseInt(binary, 2);
        String hexadecimal = Integer.toHexString(decimal);
        System.out.println("Hexadecimal equivalent: " + hexadecimal.toUpperCase());
    }
}
