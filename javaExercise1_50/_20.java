package javaExercise1_50;

// TODO: 07/02/2023 Write a Java program to convert a decimal number to hexadecimal number.
//Input Data:
//        Input a decimal number: 15
//        Expected Output
//
//        Hexadecimal number is : F

public class _20 {
    public static void main(String[] args) {
        int a = 15;

        System.out.println((Integer.toHexString(a).toUpperCase()));
        //System.out.println(Float.toHexString(Integer.parseInt(Integer.toString(a))));
    }
}
