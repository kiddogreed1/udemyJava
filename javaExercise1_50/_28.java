package javaExercise1_50;

import java.util.Scanner;

// Write a Java program to convert a hexadecimal to a decimal number. Go to the editor
// Input Data:
// Input a hexadecimal number: 25
// Expected Output

// Equivalent decimal number is: 37

public class _28 {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter a hexadecimal number: ");
    String hex = sc.nextLine();

    // Convert hexadecimal to decimal
    int decimal = 0;
    for (int i = 0; i < hex.length(); i++) {
        char c = hex.charAt(i);
        int digit = Character.digit(c, 16);
        decimal = 16 * decimal + digit;
    }

    // Output the result
    System.out.println("The decimal equivalent of " + hex + " is " + decimal);
    sc.close();

  }
}
