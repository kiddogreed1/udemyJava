package javaExercise1_50;

// TODO: 06/02/2023 Write a Java program to add two binary numbers.
//
//Input Data:
//        Input first binary number: 10
//        Input second binary number: 11
//        Expected Output
//
//        Sum of two binary numbers: 101

public class _17 {
    public static void main(String[] args) {
        int a = 1;
        int b = 1;

        String s1 = Integer.toString(a);
        String s2 = Integer.toString(b);
        int number0 = Integer.parseInt(s1, 2);
        int number1 = Integer.parseInt(s2, 2);

        int sum = number0 + number1;
        String s3 = Integer.toBinaryString(sum);
        System.out.println(s3);

    }
}
