package javaExercise1_50;

// TODO: 07/02/2023  Write a Java program to convert a binary number to decimal number.
//
//Input Data:
//        Input a binary number: 100
//        Expected Output
//
//        Decimal Number: 4
public class _22 {
    public static void main(String[] args) {
        int a = 100;
        System.out.println((Integer.toString(a)));
    }
}
