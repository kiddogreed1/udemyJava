package codingWithJohn;

public class EnumTest {

    enum Level {
        LOW,
        MEDIUM,
        HIGH,
        SPECIAL

    }

    public static void main(String[] args) {

        Level s = Level.LOW;
        Level ss = Level.MEDIUM;
        Level sss = Level.HIGH;
        Level ssss = Level.SPECIAL;
        System.out.println(s);

        for (Level l:Level.values()
             ) {
            System.out.println(l);
        }
    }
}
