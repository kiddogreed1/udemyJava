package learnProgramming;

public class _isEven {
    public static void main(String[] args) {

        int max = 100;
        int n = 66;
        int ctr = 0;

//        for (int i = 1;i <= max;i++){
//            System.out.println("number "+i+" "+isEvenNumber(i));
//        }

        while (n <= max){
            n++;
            if(!isEvenNumber(n)){continue;}
            ctr ++;
            System.out.println("even "+ n);

            if (ctr == 5){
                break;
            }
            System.out.println("found "+ctr+" even numbers");
        }
    }

    private static boolean isEvenNumber(int n){
        if (n % 2 == 0) return true;
        else return false;
    }
}
