package learnProgramming;

public class _sumDigits {
    public static void main(String[] args) {

        System.out.println(sumDigit(456));
        System.out.println(sumDigit2(456));
    }

    private static int sumDigit(int num){
        if (num <10){
            return -1;
        }
        int sum = 0;

        for (sum=0; num!=0; num=num/10){
           sum += num % 10;

        }
        return  sum;
    }
    private static int sumDigit2(int n){
       int sum = 0;
        while (n!=0){
            sum += n % 10;
            n /= 10;

        }
        return sum;
    }
}
