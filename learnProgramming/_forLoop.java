package learnProgramming;

public class _forLoop {
    public static void main(String[] args) {

        for (int i = 2; i<9;i++){
            //System.out.println("interest rate:"+i+" ");
            System.out.println(i+" percent" +String.format("%.2f",calculateInterest(10000, i)));
        }
        for (int i = 8 ; i > 1; i--){
            System.out.println(i+" percent"+String.format("%.2f",calculateInterest(10000, i)));
        }

    }
    private static double calculateInterest(int amount, double interestRate){
        return (amount *(interestRate /100));
    }
}
