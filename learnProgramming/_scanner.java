package learnProgramming;

import java.awt.*;
import java.util.Scanner;

public class _scanner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter name:");
        boolean hasNextStr = sc.hasNextInt();

        if (!hasNextStr){

            String name = sc.nextLine();
            System.out.println("enter birth year:");
            boolean hasNextInt = sc.hasNextInt();
            if(hasNextInt){

                int birthYr =  sc.nextInt();
                int age = (2023 - birthYr)-1;
                if (age > 0 && age < 75){

                    String message = String.format("Hello! Master %s, You are  %s and half yrs old.", name, age);
                    System.out.println(message);
                }else {
                    System.out.println("Invalid age");
                }

            }else{
                System.out.println("Invalid year");
            }

        }else {
            System.out.println("Invalid name");
        }






    }
}
