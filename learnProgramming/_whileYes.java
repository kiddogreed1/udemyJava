package learnProgramming;

import java.util.Scanner;

public class _whileYes {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("welcome ");

        boolean isCont = true;

        while (isCont){
            System.out.println("please enter y/n");
            String input = sc.nextLine();

            if (input.equals("y")){
                System.out.println("continue then");

            }else{
                isCont = false;
                System.out.println("goodbye");
            }

        }
    }
}
