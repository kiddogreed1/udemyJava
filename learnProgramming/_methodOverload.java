package learnProgramming;

public class _methodOverload {

    public static double  calcFeetAndInchesToCentimeters(double feet, double inches) {
        if (feet<0 || (inches<0 || inches> 12) ){
            System.out.println("Error");
            return -1;
        }

        double cm = (feet * 12)* 2.54;
        cm += inches *2.54;
        System.out.println(feet +" ft, "+inches+" inch = "+cm+" cm");
        return 0;
    }

    public static void main(String[] args) {
        //System.out.println(calcFeetAndInchesToCentimeters(1,1));
        calcFeetAndInchesToCentimeters(6,-5);
    }

}


