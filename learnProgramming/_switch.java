package learnProgramming;

public class _switch {
    public static void main(String[] args) {
        String test = "test";
        System.out.println(test.toUpperCase());

        char x = 'F';
        switch (x){
            case 'A':
                System.out.println("A");
                break;
            case 'B':
                System.out.println("B");
                break;
            case 'C':
                System.out.println("C");
                break;
            case 'D':
                System.out.println("D");
                break;
            case 'E' : case 'F':
                System.out.println("E or F");
                break;
            default:
                System.out.println("unreachable");
        }
    }
}
