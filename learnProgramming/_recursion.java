package learnProgramming;

public class _recursion {
    public static void main(String[] args) {

        int number = 6;
        int result = factorial(number);
        System.out.println("The factorial of " + number + " is: " + result);

    }

    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        } else {
            System.out.println(n * factorial(n-1));
            return n * factorial(n-1);
        }
    }
}
