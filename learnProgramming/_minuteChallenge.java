package learnProgramming;

public class _minuteChallenge {

    private static final String INVALID_MESSAGE = "Invalid Message";
    private static String getDurationString(long minutes,long seconds){

        if ((minutes < 0)|| (seconds <0)||(seconds>59)){
            return INVALID_MESSAGE;
        }

        long hr = minutes/60;
        long remainingMins = minutes % 60;

        String hrString = hr+"H:";
        if(hr<10){
             hrString = "0"+hrString;
        }
        String minString = remainingMins+"M:";
        if(remainingMins<10){
             minString = "0"+minString;
        }
        String secString = seconds+"S";
        if(seconds<10){
             secString = "0"+secString;
        }
        return hrString+minString+secString;
    }
    private static String getDurationString(long seconds){
        if (seconds < 0){
            return INVALID_MESSAGE;
        }
        long minutes = seconds / 60;
        long remainingSec = seconds % 60;
        return getDurationString(minutes,remainingSec);
    }

    public static void main(String[] args) {
        System.out.println(getDurationString(65,45));
        System.out.println(getDurationString(3945L));
        System.out.println(getDurationString(-53));
    }
}
