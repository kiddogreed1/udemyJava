package learnProgramming;

import java.util.Scanner;

public class _scannerChallenge {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String greeting = "Hello please enter number::";


        int sum = 0;
        int ctr = 1;
        while (ctr <=10){
            System.out.println(greeting +" attempt #"+ctr);
            boolean hasNextIn = sc.hasNextInt();
            if (!hasNextIn){
                System.out.println("Invalid input");
                break;
            }
            int input = sc.nextInt();
            sum += input;
            System.out.println(sum);
            ctr++;


        }
        System.out.println("total of "+sum+" with "+ (ctr-1) +" attempts");

    }


}
