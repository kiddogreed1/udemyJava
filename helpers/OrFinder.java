package helpers;

public class OrFinder {
  
  
    //   public static String filterString(String input) {
    //     String[] words = input.split(" ");
        
    //     for (String word : words) {
    //         if (word.contains(" OR ")) {
    //             return word.split(" OR ")[0];
    //         }
    //     }
        
    //     return null; // Return null if no match is found
    // }

    //   public static String filterWords(String words) {
    //     for (String word : words) {
    //         if (word.contains(" OR ")) {
    //             return word;
    //         }
    //     }
    //     return null; // Return null if no word contains " OR "
    // }


    //   public static String filterStringOr(String input) {
    //     String[] words = input.split("\\s+"); // Split the input string into words
    //     for (String word : words) {
    //       //System.out.println(word);
    //         if (word.contains(" OR ")) {
            
    //             return word; // Return the word if it contains " OR "
    //         }
    //     }
      
    //     return null; // Return null if no word contains " OR "
    // }

    //   public static String filterString(String input) {
    //     if (input.contains("OR")) {
    //         return input; // Return the input string if it contains "OR"
    //     }
    //     return null; // Return null if the input string does not contain "OR"
    // }


      public static String filterings(String input) {
        String[] words = input.split("\\s+"); // Split the input string into words
        StringBuilder filteredString = new StringBuilder();
        
        if (input.length() <= 20) {
            return filteredString.toString().trim()+ " ";
        } else {
            
            boolean foundOR = false;
            for (String word : words) {
                filteredString.append(word).append(" ");
                if (word.equals("OR")|| word.contains("AND")) {
                    
                // orAnd = (word.equals("AND")|| word.equals("orAnd"))? " AND" : " OR";
            
                    foundOR = true;
                    break;
                }
            
            }

            if (foundOR) {
                return filteredString.toString().trim()+ " ";
            } else {
                return " ";
            }
        }
        
       // String orAnd = " ";
     
    }

    public static void main(String[] args) {
        String input1 = "Hello OR World";
        String input2 = "Hello World test OR";
        String input3 = " Hello OR World test ";
        String input4 = " OR Hello World test OR";
        String input5 = "Hello World OR test OR";
        String input6 = "MENELEO ESTANISLAO RAMOS OR ANNA VICTORIA BELEN RAMOS OR ANDREA BEATRICE BELEN RAMOS";
        String input7 = "MENELEO ESTANISLAO AND RAMOS OR ANNA VICTORIA BELEN RAMOS OR ANDREA BEATRICE BELEN RAMOS";
        String input8 = "tom AND Jerry";
        String input9 = "dong OR johng";


        System.out.println(filterings(input1)); // Output: Hello
        System.out.println(filterings(input2)); // Output: Hello World
        System.out.println(filterings(input3));
        System.out.println(filterings(input4));
        System.out.println(filterings(input5));
        System.out.println(filterings(input6));
        System.out.println(filterings(input7));
        System.out.println(filterings(input8));
        System.out.println(filterings(input9));
    }
}

