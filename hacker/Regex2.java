package hacker;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Regex2 {

    public static void main(String[] args) {

        String text = "5\n" +
                "Goodbye bye bye world world world\n" +
                "Sam went went to to to his business\n" +
                "Reya is is the the best player in eye eye game\n" +
                "in inthe\n" +
                "Hello hello Ab aB";
        String regex = "\\b(\\w+)\\b(?:.*\\b\\1\\b)+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            System.out.println("Repeated word: " + matcher.group(1));
        }

    }





}
