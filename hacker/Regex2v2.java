import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex2v2 {
    public static void main(String[] args) {
        String text = "Goodbye bye bye world world world\n" +
                "Sam went went to to to his business\n" +
                "Reya is is the the best player in eye eye game\n" +
                "in inthe\n" +
                "Hello hello Ab aB";
        String regex = "\\b(\\w+)\\b(?:.*\\b\\1\\b)+";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            String repeatedWord = matcher.group(1);
            text = text.replaceAll("\\b" + repeatedWord + "\\b", "REPEATED");
            System.out.println(text);
        }
    }
}
