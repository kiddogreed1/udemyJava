package hacker;

import java.io.*;
import java.util.*;

public class _staticBlock {



        static int B;
        static int H;
        static boolean flag = true;

        static {
            Scanner scan = new Scanner(System.in);
            B = scan.nextInt();
            H = scan.nextInt();
            scan.close();
            if (B <= 0 || H <= 0) {
                flag = false;
                System.out.println("java.lang.Exception: Breadth and height must be positive");
            }
        }

        public static void main(String[] args) {
            /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

            if (flag) {
                int area = B * H;
                System.out.print(area);
            }
        }


}
