package javaLoop;

import java.util.Scanner;

// Question 3

// Write a program that prompts the user to input a positive integer.
//  It should then print the multiplication table of that number. 

public class _3 {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.println("Enter a number and the ai will generate the multiplication table:");
    boolean hasNextInt = sc.hasNextInt();
      if (!hasNextInt) {
        System.out.println("Invalid number");
        
      }
    int inputNumber = sc.nextInt();
    System.out.println("Enter a number of limits the ai will generate:");
      if (!hasNextInt) {
        System.out.println("Invalid number");
      }
    int limit = sc.nextInt();
      
    
    System.out.println(multiplicationTable(inputNumber, limit));
    
  }
  
  private static String multiplicationTable(int number, int limit) {
    System.out.println(" Multiplication Table "+ number);
    for (int i = 1; i <= limit; i++) {
        System.out.println(" " + i + " x " +number+" = "+(i*number));
    }
    return " ";
    
  }
}
