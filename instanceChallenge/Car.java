package instanceChallenge;

public class Car extends Vehicle {

  private int numDoors;
  private int wheelCount;
  private int gears;
  private boolean isManual;

  private  int currentGear;


    
    public Car(String manufacturer, String model, int year, int numDoors, int wheelCount, int gears, boolean isManual) {
    super(manufacturer, model, year);
    this.numDoors = numDoors;
    this.wheelCount = wheelCount;
    this.gears = gears;
    this.isManual = isManual;
    this.currentGear = 1;
  }

    public void unlockDoors() {
      System.out.println("Doors unlocked.");
  }

    public void lockDoors() {
        System.out.println("Doors locked.");
    }

    public void changeGear(int currentGear) {
      this.currentGear = currentGear;
      System.out.println("Car current gear"+ this.currentGear);
    }

    public void changeVelocity(int speed, int direction) {
      move(speed, direction);
      System.out.println("car changed velocity" +speed + "direction"+direction);
    }


    

    
}
