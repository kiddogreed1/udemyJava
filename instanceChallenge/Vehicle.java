package instanceChallenge;



public class Vehicle {

  private String manufacturer;
  private String model;
  private int year;
  private int currentVelocity;
  private int currentDirection;

  public Vehicle(String manufacturer, String model, int year) {
    this.manufacturer = manufacturer;
    this.model = model;
    this.year = year;

    this.currentDirection = 0;
    this.currentVelocity = 0;

    

  }

      
    public void startEngine() {
        System.out.println("Engine started.");
    }

    public void stopEngine() {
        System.out.println("Engine stopped.");
    }
    
    public void steer(int direction) {
      this.currentDirection += direction;
      System.out.println("Steering direction at " + this.currentDirection + " degrees.");
    }

    public void move (int direction, int velocity) {
      currentVelocity = velocity;
      currentDirection = direction;
      System.out.println("moving to " + this.currentDirection + " at " +currentVelocity+ "speed");
    }

    public void stop(){
      currentVelocity = 0;
    }



  
}
