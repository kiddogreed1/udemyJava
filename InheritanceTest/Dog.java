package InheritanceTest;

public class Dog  extends Animal{

    private int eyes;
    private int legs;
    private int tails;
    private int teeth;


    public Dog(String name, int brain, int body, int size, int weight) {
        super(name, brain, body, size, weight);
    }
}
