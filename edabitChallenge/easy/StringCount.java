package edabitChallenge.easy;

public class StringCount {

    public static int countWords(String s) {

        int count = 0;
        for (String retval : s.split(" ")) {
            count++;
        }
        return count;
    }
}


