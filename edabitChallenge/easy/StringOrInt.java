package edabitChallenge.easy;

public class StringOrInt {
    public static String intOrString(Object var) {
        // TODO: 28/05/2023 https://stackoverflow.com/questions/4989818/instanceof-vs-getclass
        //

        if (var instanceof String) {
            return "String";
        } else if (var instanceof Integer) {
            return "int";
        } else {
            return "Unknown type";
        }




    }

    //code below is the shorten version
    public static String checkType(Object value) {
        return value instanceof String ? "String" :
                value instanceof Integer ? "int" : "Unknown type";
    }
}
