package edabitChallenge.easy;

public class ManyOperators {
    public static int operate(int num1, int num2, String operator) {
        // TODO: 28/05/2023
        //  Some basic arithmetic operators are +, -, *, /, and %. In this challenge you will be given three parameters, num1, num2, and an operator. Use the operator on parameters num1 and num2.
        //  There will be no division by zero.
        switch (operator){
            case "+":
                return num1+num2;
            case "-":
                return num1-num2;
            case "*":
                return num1*num2;
            case "/":
                if(num1 == 0 || num2 == 0){return 0;}
                return num1/num2;
            case "%":
                return num1%num2;
            default:
                throw new IllegalStateException("Unexpected value: " + operator);
        }

    }


}
