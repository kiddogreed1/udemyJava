package edabitChallenge.easy;

public class BasicCalculator {
    public static int calculator(int num1, char operator, int num2){
// TODO: 28/05/2023 basic calculate
//  If the input tries to divide by 0, return 0.

        switch (operator){
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            case '/':
                if (num2 == 0 || num1 == 0)return 0;
                return num1 / num2;
            case '*':
                return num1 * num2;

            default:
                throw new IllegalStateException("Unexpected value: " + operator);
        }

    }

    public static int calc(int x,  int y, double z){

        x = x + 2;
        z = x + y -7;
        y = x * 3;

        System.out.println("x="+x);
        System.out.println("y="+y);
        System.out.println("Z= "+z);
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(calc(0,4,3));


    }
}
