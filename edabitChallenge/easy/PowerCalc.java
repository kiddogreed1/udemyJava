package edabitChallenge.easy;

public class PowerCalc {

    public int calculatePower(int voltage, int power){
        return voltage * power;
    }
}
