package edabitChallenge.easy;

public class NumberSequence {
    public static int triangle(int n) {

        int ctr = 1;
        for (int i = 0; i<n; ++i) {
         if (i == 1){
             continue;
         }
         ctr+= i;
        }
        return ctr;
    }

    public static void main(String[] args) {
        int test = triangle((6));
       // System.out.println(test);
    }
}
