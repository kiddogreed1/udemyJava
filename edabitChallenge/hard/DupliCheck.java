package edabitChallenge.hard;

import java.util.HashMap;
import java.util.Map;

public class DupliCheck {
  
  public static int duplicateCount(String str) {
    

     // Create a map to store character counts
     Map<Character, Integer> charCounts = new HashMap<>();
     // Loop through each character in the string
     for (char c : str.toCharArray()) {
         // Ignore non-alphanumeric characters
         if (!Character.isLetterOrDigit(c)) {
             continue;
         }
         // If we've already seen this character before, increment its count
         if (charCounts.containsKey(c)) {
             charCounts.put(c, charCounts.get(c) + 1);
         } else {  // Otherwise add it to the map with an initial count of 1.
             charCounts.put(c, 1);
         }
     }
     int duplicateCount = 0;
     for (int count : charCounts.values()) {
        //If the value is greater than one then there are duplicates.
          if (count > 1) {
              duplicateCount++;
          }
      }
      return duplicateCount;
  }
}
