package edabitChallenge.hard;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import learnProgramming.speedConveter;

public class Regex {
  
  public static void main(String[] args) {

    System.out.println(validate("121317"));
    System.out.println(validate("1234"));
    
    System.out.println(validate("45135"));
    System.out.println(validate("89abc1"));
    System.out.println(validate("900876"));
    System.out.println(validate(" 4983"));








    
  }

  public static boolean validate(String pin) {
		
     // A valid PIN should be exactly 4 or 6 digits long
     String regex = "^[0-9]{4}$|^[0-9]{6}$";
     // Create a Pattern object with the regex and compile it
     Pattern pattern = Pattern.compile(regex);
     // Use Matcher to match input against the compiled regex pattern.
     Matcher matcher = pattern.matcher(pin);
     // Return true if there is a match, false otherwise.  
      return matcher.matches();
  }


}
