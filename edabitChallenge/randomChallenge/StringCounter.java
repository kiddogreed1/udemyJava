package edabitChallenge.randomChallenge;

import java.util.HashMap;
import java.util.Map;

/**
 * StringCounter
 */
public class StringCounter {

  public static int countDuplicates(char[] array) {
    // Create a HashMap to store the count of each character
    Map<Character, Integer> countMap = new HashMap<>();
    
    // Iterate through the array and count the occurrences of each character
    for (char c : array) {
        countMap.put(c, countMap.getOrDefault(c, 0) + 1);
    }
    
    int duplicateCount = 0;
    
    // Iterate through the countMap and count the number of duplicates
    for (int count : countMap.values()) {
        if (count > 1) {
            duplicateCount++;
        }
    }
    
    return duplicateCount;
}

public static void main(String[] args) {
  
  char[] array = {'a', 'b', 'c', 'a', 'b', 'd', 'e', 'e'};
  int duplicateCount = countDuplicates(array);
  System.out.println("Number of duplicate values: " + duplicateCount);

}

  
}