package edabitChallenge.medium;

import java.text.DecimalFormat;
import java.util.Arrays;

public class OtherSides {
    private static final DecimalFormat decfor = new DecimalFormat("0.00");
    public static void main(String[] args) {

        // TODO: 03/06/2023 30° by 60° by 90° triangles always follow this rule: let's say the shortest side length is x units, the hypotenuse would be 2x units and the other side would be x * square root of 3.
        //The results in the Tests are rounded up to 2 decimal places.
        //Return the result as an array.

        System.out.println(Arrays.toString(otherSides(1)));

    }
    public static double[] otherSides(int n) {

        double midle = n * 2;
        double last =  n * (Math.sqrt(3));
        System.out.printf("%.2f%n",last);
        double[] sample;
        sample = new double[]{ midle, last };

        // Return statement of the method.
        return sample;

    }
}
