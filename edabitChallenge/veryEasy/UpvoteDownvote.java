package edabitChallenge.veryEasy;

public class UpvoteDownvote {

    public static int getVoteCount(int upvotes, int downvotes){
        return upvotes - downvotes;
    }
}
