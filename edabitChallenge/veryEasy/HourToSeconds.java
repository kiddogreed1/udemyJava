package edabitChallenge.veryEasy;

public class HourToSeconds {
    public static int howManySeconds(int hrs){
        return ((hrs * 60)*60);

    }

    public static void main(String[] args) {
       int result = howManySeconds(2);
        System.out.println(result);
    }
}
