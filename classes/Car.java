package classes;

public class Car {

    private int doors;
    private String model;
    private String color;
    private int wheel;

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
