package classes;

public class BankAccount {
    private int accountNumber;
    private int balance;
    private String customerName;
    private String email;
    private int phoneNumber;

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;

    }
    public String depositFund(int funds){
        if(funds > 100000 || funds < 100){
            System.out.println("Invalid amount");
            return "-1";
        }
        this.balance += funds;
        System.out.println(this.customerName+ " is depositing:"+this.balance+"+"+ funds+ ".00 funds");
        return "";

    }

    public String withrawFund(int funds){
        if (balance < funds){
            System.out.println("invalid transaction");
            return "-1";
        }
        this.balance -= funds;
        System.out.println(this.customerName+ " is withrawing:"+this.balance+" - "+ funds+ ".00 funds");
        System.out.println(this.customerName+ " balance:"+ this.balance+ ".00");
        return "";
    }

    public String infoCheck(){
        System.out.println("account No:"+this.accountNumber);
        System.out.println("name:"+this.customerName);
        System.out.println("phone No:"+this.phoneNumber);
        System.out.println("email:"+this.email);
        System.out.println("balance:"+this.balance);
        return "";
    }
}
