package javaArray;

public class ArrayTest {
    public static void main(String[] args) {
        //declaring array
        double[] data;

        //allocating memory
        data = new double[10];

        //declaring and allocating with max of 10
        String[] names = new String[10];

        //initializing array [1,2,3,4,5]
        int[] nums = {1,2,3,4,5};
        System.out.println(data);
        System.out.println(names);
        System.out.println(nums);
        //iterate to display array values
        for (int n:nums
             ) {
            System.out.println(n);
        }

        // declare an array
        int[] age = new int[5];

        //other ways to initialize array
                age[0] = 12;
                age[1] = 4;
                age[2] = 5;


    }
}
