package javaArray;

public class Multidimensional {
    public static void main(String[] args) {

        //sample
        double[][] matrix = {{1.2, 4.3, 4.0},
                {4.1, -1.1}
        };

      //  System.out.println(matrix);
        for (double[] mat:matrix
             ) {
          //  System.out.println(mat);
            for (double m:mat
                 ) {
                System.out.print(" ["+m+"] ");
            }
        }
    }
}
