package javaArray;

public class ComputeSum {
    public static void main(String[] args) {

        int[] nums = {2, -9, 0, 5, 12, -25, 22, 9, 8, 12};
        int sum = 0;
        double average;

        // access all elements using for each loop
        // add each element in sum
        for (int n:nums
             ) {
            sum+=n;
        }

        // get the total number of elements
        int maxLength = nums.length;

        // calculate the average
        average = ((double)sum / (double)maxLength);
        for (int n:nums
        ) {
            System.out.print(" ["+n+"] ");
        }
        System.out.println("Sum = " + sum);
        System.out.println("Average = " + average);




    }
}
