package javaConditional;

import java.util.Scanner;

public class _Oop{


    public class WeekdayPicker {
        private static final String[] WEEKDAYS = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            displayMessage();
            int userInput = getUserInput(sc);
            validateInput(userInput);
            printWeekday(userInput);
        }

        private static void displayMessage() {
            String message = """
                    Basic date picker for one week. Please enter a number:
                    """;
            System.out.println(message);
        }

        private static int getUserInput(Scanner scanner) {
            if (!scanner.hasNextInt()) {
                throw new IllegalStateException("Invalid input. Only numeric input is allowed.");
            }
            return scanner.nextInt();
        }

        private static void validateInput(int userInput) {
            if (userInput < 0) {
                throw new IllegalStateException("Negative numbers are not allowed.");
            }
            if (userInput > 7) {
                throw new IllegalArgumentException("Numbers greater than 7 are not allowed.");
            }
            if (userInput == 0) {
                userInput += 1;
            }
        }

        private static void printWeekday(int userInput) {
            System.out.println(WEEKDAYS[userInput - 1]);
        }

    }

}
