package javaConditional;

import java.util.Scanner;

public class _1 {
  // Write a Java program to get a number from the user and print whether it is positive or negative.

  public static void main(String[] args) {

    boolean isOn = true;
    

    while (isOn) {
      int ctr = 0;

      if(ctr == 5)isOn = false;
      String message = """
        please enter a number and the ai will tell if it is positive or negative
        """;

        System.out.println(message);
        Scanner sc = new Scanner(System.in);
        int userInput = sc.nextInt();
        boolean isValid = sc.hasNextInt();
        //validator
        if(!isValid){
         
          throw new IllegalArgumentException();
          
        }
        String answer;
        if (userInput > 0) {
          answer = "POSITIVE";
        }else{
          answer = "NEGATIVE";
        }
        System.out.println(String.format("the answer is %s",answer));
        ctr++;
    }
    
      
       
  }
}
