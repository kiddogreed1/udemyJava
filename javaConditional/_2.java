package javaConditional;

import java.util.Scanner;

public class _2 {
  
  public static void main(String[] args) {
    
    String message = """
        basic date picker for 1 week, please input  number:
        """;

        System.out.println(message);    
    Scanner sc = new Scanner(System.in);
    boolean isValid = sc.hasNextInt();
    if(!isValid)throw new IllegalStateException("invalid input it must be a number input");
    
   
    int userInput = sc.nextInt();
    if(userInput < 0)throw new IllegalStateException("negative is not allowed");

    if(userInput > 7)throw new IllegalArgumentException(" greater than 7 is not allowed");
    
    if (userInput ==0) userInput+=1;

    switch (userInput) {
      case 1:
        System.out.println("Monday");
        break;
      case 2:
        System.out.println("Tuesday");
        break;
      case 3:
        System.out.println("Wednesday");
        break;
      case 4:
        System.out.println("Thursday");
        break;
      case 5:
        System.out.println("Friday");
        break;
      case 6:
        System.out.println("Saturday");
        break;
      case 7:
        System.out.println("Sunday");
        break;  
    
      default:
        System.out.println("unknown date");
        break;
    }
  }
}
