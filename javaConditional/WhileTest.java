package javaConditional;

import java.util.Scanner;

public class WhileTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sum = 0;
        System.out.println("Enter a number");
        int Input = sc.nextInt();

        while (sum<=100){
            sum += Input;
            System.out.println("current :"+sum+ " Enter a number");
            Input = sc.nextInt();
        }
        System.out.println("Finale = " + Input);
        sc.close();
    }
}
