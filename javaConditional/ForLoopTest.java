package javaConditional;

import java.util.Scanner;

public class ForLoopTest {

    public static String loopers(int input){
        for (int i = 0; i <= input; i++) {
            if(i%2 == 0){
                System.out.println(i);
            }else{
                System.out.print(i+" ::");
            }

        }
        return "max input is: "+input;

    }
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("enter number for maximum loops");
        int userInput = sc.nextInt();
        System.out.println(loopers(userInput));
    }


}
