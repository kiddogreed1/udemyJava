package javaConditional;

import java.util.Scanner;

public class BreakTest {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String msg = "this is to test break statement";
        System.out.println(msg);
        double stdin = sc.nextDouble();
        adder(stdin);

    }

    public static String adder(Double userInput){
        double number, sum = 0.0;
        while (true){
            System.out.println("sum: "+sum);
            System.out.println("Enter number :");
            number = userInput;
            if (number < 0.0) {
                System.out.println("You enter negative number ");
                break;
            }
            if(sum >= 100){
                return "max reach";
            }
            sum += number;
        }
        System.out.println("Sum:"+sum);
        return "total is "+ userInput;
    }
}
