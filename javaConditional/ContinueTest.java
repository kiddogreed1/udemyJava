package javaConditional;

import java.util.Scanner;

public class ContinueTest {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String message = "Skipper of number enter max number:";

        int first, second ;
        System.out.println(message);
        int max = sc.nextInt();
        while (true){
            System.out.println("enter 1st number:");
            first = sc.nextInt();
            System.out.println("enter 2nd number:");
            second = sc.nextInt();

            for (int i = 1; i < max; i++) {
                if (i > first && i < second) {
                    continue;
                }
                System.out.println(i);

            }
            break;
        }


    }
    public static String skipper(int first, int second){

        return "skipped between "+first +" and "+second;
    }
}

